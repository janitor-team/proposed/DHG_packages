Source: haskell-esqueleto
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends: debhelper (>= 10),
 haskell-devscripts (>= 0.13),
 cdbs,
 ghc (>= 8.4.3),
 ghc-prof,
 libghc-aeson-dev (>= 1.0),
 libghc-aeson-prof,
 libghc-attoparsec-dev (>= 0.13),
 libghc-attoparsec-dev (<< 0.14),
 libghc-attoparsec-prof,
 libghc-blaze-html-dev,
 libghc-blaze-html-prof,
 libghc-conduit-dev (>= 1.3),
 libghc-conduit-prof,
 libghc-monad-logger-dev,
 libghc-monad-logger-prof,
 libghc-persistent-dev (>= 2.10.0),
 libghc-persistent-dev (<< 2.11),
 libghc-persistent-prof,
 libghc-resourcet-dev (>= 1.2),
 libghc-resourcet-prof,
 libghc-tagged-dev (>= 0.2),
 libghc-tagged-prof,
 libghc-unliftio-dev,
 libghc-unliftio-prof,
 libghc-unordered-containers-dev (>= 0.2),
 libghc-unordered-containers-prof,
 libghc-exceptions-dev,
 libghc-exceptions-prof,
 libghc-hspec-dev,
 libghc-hspec-prof,
 libghc-persistent-sqlite-dev,
 libghc-persistent-sqlite-prof,
 libghc-persistent-template-dev,
 libghc-persistent-template-prof,
 libghc-vector-dev,
 libghc-vector-prof,
Build-Depends-Indep: ghc-doc,
 libghc-aeson-doc,
 libghc-attoparsec-doc,
 libghc-blaze-html-doc,
 libghc-conduit-doc,
 libghc-monad-logger-doc,
 libghc-persistent-doc,
 libghc-resourcet-doc,
 libghc-tagged-doc,
 libghc-unliftio-doc,
 libghc-unordered-containers-doc,
Standards-Version: 4.5.0
Homepage: https://github.com/bitemyapp/esqueleto
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-esqueleto
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-esqueleto]
X-Description: type-safe EDSL for SQL on persistent backends
 esqueleto is a bare bones, type-safe EDSL for SQL queries
 that works with unmodified persistent SQL backends.  Its
 language closely resembles SQL, so you don't have to learn
 new concepts, just new syntax, and it's fairly easy to
 predict the generated SQL and optimize it for your backend.
 Most kinds of errors committed when writing SQL are caught as
 compile-time errors---although it is possible to write
 type-checked esqueleto queries that fail at runtime.

Package: libghc-esqueleto-dev
Architecture: any
Depends: ${haskell:Depends},
 ${shlibs:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-esqueleto-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-esqueleto-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
