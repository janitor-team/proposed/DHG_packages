Source: haskell-uuid
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-cryptohash-md5-dev (>= 0.11.100),
 libghc-cryptohash-md5-dev (<< 0.12),
 libghc-cryptohash-md5-prof,
 libghc-cryptohash-sha1-dev (>= 0.11.100),
 libghc-cryptohash-sha1-dev (<< 0.12),
 libghc-cryptohash-sha1-prof,
 libghc-entropy-dev (>= 0.3.7),
 libghc-entropy-dev (<< 0.5),
 libghc-entropy-prof,
 libghc-network-info-dev (>= 0.2),
 libghc-network-info-dev (<< 0.3),
 libghc-network-info-prof,
 libghc-quickcheck2-dev (>= 2.4),
 libghc-random-dev (<< 1.2),
 libghc-random-dev (>= 1.0.1),
 libghc-random-prof,
 libghc-hunit-dev (>= 1.2),
 libghc-tasty-dev (>= 0.10),
 libghc-tasty-hunit-dev (>= 0.9),
 libghc-tasty-quickcheck-dev (>= 0.8),
 libghc-uuid-types-dev (<< 2),
 libghc-uuid-types-dev (>= 1.0.2),
 libghc-uuid-types-prof,
Build-Depends-Indep: ghc-doc,
 libghc-cryptohash-md5-doc,
 libghc-cryptohash-sha1-doc,
 libghc-entropy-doc,
 libghc-network-info-doc,
 libghc-random-doc,
 libghc-uuid-types-doc,
Standards-Version: 4.5.0
Homepage: https://github.com/hvr/uuid
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-uuid
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-uuid]

Package: libghc-uuid-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: create, compare, parse and print Universally Unique Identifiers${haskell:ShortBlurb}
 This library is useful for creating, comparing, parsing and printing
 Universally Unique Identifiers. See http://en.wikipedia.org/wiki/UUID
 for the general idea.
 .
 ${haskell:Blurb}

Package: libghc-uuid-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: create, compare, parse and print UUIDs${haskell:ShortBlurb}
 This library is useful for creating, comparing, parsing and printing
 Universally Unique Identifiers. See http://en.wikipedia.org/wiki/UUID
 for the general idea.
 .
 ${haskell:Blurb}

Package: libghc-uuid-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: create, compare, parse and print UUIDs${haskell:ShortBlurb}
 This library is useful for creating, comparing, parsing and printing
 Universally Unique Identifiers. See http://en.wikipedia.org/wiki/UUID
 for the general idea.
 .
 ${haskell:Blurb}
