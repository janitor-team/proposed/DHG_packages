Source: haskell-blogliterately
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Ilias Tsitsimpis <iliastsi@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-blaze-html-dev (>= 0.5),
 libghc-blaze-html-dev (<< 0.10),
 libghc-blaze-html-prof,
 libghc-bool-extras-dev,
 libghc-bool-extras-prof,
 libghc-cmdargs-dev (<< 0.11),
 libghc-cmdargs-dev (>= 0.9.5),
 libghc-cmdargs-prof,
 libghc-data-default-dev (<< 0.8),
 libghc-data-default-dev (>= 0.5),
 libghc-data-default-prof,
 libghc-haxml-dev (<< 1:1.26),
 libghc-haxml-dev (>= 1:1.22),
 libghc-haxml-prof,
 libghc-haxr-dev (<< 3000.12),
 libghc-haxr-dev (>= 3000.11),
 libghc-haxr-prof,
 libghc-highlighting-kate-dev (<< 0.7),
 libghc-highlighting-kate-dev (>= 0.5),
 libghc-highlighting-kate-prof,
 libghc-hscolour-dev (<< 1.25),
 libghc-hscolour-dev (>= 1.20),
 libghc-hscolour-prof,
 libghc-http-dev (<< 1:4000.4),
 libghc-http-dev (>= 1:4000.3),
 libghc-http-prof,
 libghc-lens-dev (>= 3.8),
 libghc-lens-dev (<< 4.20),
 libghc-lens-prof,
 libghc-pandoc-dev (>= 2.0),
 libghc-pandoc-dev (<< 2.11),
 libghc-pandoc-prof,
 libghc-pandoc-citeproc-dev (>= 0.1.2),
 libghc-pandoc-citeproc-dev (<< 0.18),
 libghc-pandoc-citeproc-prof,
 libghc-pandoc-types-dev (>= 1.20),
 libghc-pandoc-types-dev (<< 1.22),
 libghc-pandoc-types-prof,
 libghc-split-dev (<< 0.3),
 libghc-split-dev (>= 0.1.4),
 libghc-split-prof,
 libghc-strict-dev (<< 0.4),
 libghc-strict-dev (>= 0.3),
 libghc-strict-prof,
 libghc-tagsoup-dev (<< 0.15),
 libghc-tagsoup-dev (>= 0.13.4),
 libghc-tagsoup-prof,
 libghc-temporary-dev (>= 1.1),
 libghc-temporary-dev (<< 1.4),
 libghc-temporary-prof,
 pandoc,
Build-Depends-Indep: ghc-doc,
 libghc-http-doc,
 libghc-haxml-doc,
 libghc-blaze-html-doc,
 libghc-bool-extras-doc,
 libghc-cmdargs-doc,
 libghc-data-default-doc,
 libghc-haxml-doc,
 libghc-haxr-doc,
 libghc-highlighting-kate-doc,
 libghc-hscolour-doc,
 libghc-http-doc,
 libghc-lens-doc,
 libghc-pandoc-citeproc-doc,
 libghc-pandoc-doc,
 libghc-pandoc-types-doc,
 libghc-split-doc,
 libghc-strict-doc,
 libghc-tagsoup-doc,
 libghc-temporary-doc,
Standards-Version: 4.5.0
Homepage: https://byorgey.wordpress.com/blogliterately/
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-blogliterately
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-blogliterately]
X-Description: Tool for posting articles to blogs (internals)
 This package provides development internals of BlogLiterately tool,
 allowing you write blog posts in Markdown format, then use it to do
 syntax highlighting, format ghci sessions, and upload to any blog
 supporting the metaWeblog API (such as Wordpress)
 .
 This package is made available to make customization possible,
 in particular, to create your own executable which adds extra custom
 transformations.
 .
 End-user is probably interested in `blogliterately' package, which
 provides a ready to use executable.

Package: libghc-blogliterately-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Replaces:
 ${haskell:Replaces},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-blogliterately-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Replaces:
 ${haskell:Replaces},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-blogliterately-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Replaces:
 ${haskell:Replaces},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: blogliterately
Architecture: any
Section: web
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Replaces:
 ${haskell:Replaces},
Description: Tool for posting Haskelly articles to blogs
 This package provides BlogLiterately executable, which allows you to
 write blog posts in Markdown format, then use it to do syntax
 highlighting, format ghci sessions, and upload to any blog supporting
 the metaWeblog API (such as Wordpress)
