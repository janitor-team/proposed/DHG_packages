Source: haskell-language-haskell-extract
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Kiwamu Okabe <kiwamu@debian.or.jp>,
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-regex-posix-dev,
 libghc-regex-posix-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-regex-posix-doc,
Standards-Version: 4.5.0
Homepage: https://github.com/finnsson/template-helper
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-language-haskell-extract
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-language-haskell-extract]

Package: libghc-language-haskell-extract-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: automatically extract functions from the local code.
 language-haskell-extract contains some useful helper functions on top of
 Template Haskell.
 functionExtractor extracts all functions after a regexp-pattern.
 .
 > foo = "test"
 > boo = "testing"
 > bar = $(functionExtractor "oo$")
 .
 will automagically extract the functions ending with "oo" such as
 .
 > bar = [("foo",foo), ("boo",boo)]
 .
 This can be useful if you wish to extract all functions beginning with test
 (for a test-framework) or all functions beginning with wc (for a web service).
 .
 functionExtractorMap works like functionsExtractor but applies a function
 over all function-pairs.
 This functions is useful if the common return type of the functions is
 a type class.
 .
 This package contains the normal library files.

Package: libghc-language-haskell-extract-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: automatically extract functions from the local code.; profiling libraries
 language-haskell-extract contains some useful helper functions on top of
 Template Haskell.
 functionExtractor extracts all functions after a regexp-pattern.
 .
 > foo = "test"
 > boo = "testing"
 > bar = $(functionExtractor "oo$")
 .
 will automagically extract the functions ending with "oo" such as
 .
 > bar = [("foo",foo), ("boo",boo)]
 .
 This can be useful if you wish to extract all functions beginning with test
 (for a test-framework) or all functions beginning with wc (for a web service).
 .
 functionExtractorMap works like functionsExtractor but applies a function
 over all function-pairs.
 This functions is useful if the common return type of the functions is
 a type class.
 .
 This package contains the libraries compiled with profiling enabled.

Package: libghc-language-haskell-extract-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: automatically extract functions from the local code.; documentation
 language-haskell-extract contains some useful helper functions on top of
 Template Haskell.
 functionExtractor extracts all functions after a regexp-pattern.
 .
 > foo = "test"
 > boo = "testing"
 > bar = $(functionExtractor "oo$")
 .
 will automagically extract the functions ending with "oo" such as
 .
 > bar = [("foo",foo), ("boo",boo)]
 .
 This can be useful if you wish to extract all functions beginning with test
 (for a test-framework) or all functions beginning with wc (for a web service).
 .
 functionExtractorMap works like functionsExtractor but applies a function
 over all function-pairs.
 This functions is useful if the common return type of the functions is
 a type class.
 .
 This package contains the documentation files.
