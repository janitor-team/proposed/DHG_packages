haskell-ghc-mtl (1.2.1.0-10) unstable; urgency=medium

  * Sourceful upload for GHC 8.8

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 14 Jun 2020 13:04:09 +0300

haskell-ghc-mtl (1.2.1.0-9) unstable; urgency=medium

  * Remove build dependency on libghc-mtl-dev (provided by ghc-8.4.3)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Mon, 01 Oct 2018 13:33:25 +0300

haskell-ghc-mtl (1.2.1.0-8) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:09:34 +0300

haskell-ghc-mtl (1.2.1.0-7) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:04:50 -0400

haskell-ghc-mtl (1.2.1.0-6) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:33:48 -0400

haskell-ghc-mtl (1.2.1.0-5) experimental; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)
  * Convert `debian/copyright' to dep5 format

  [ Clint Adams ]
  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Sun, 16 Oct 2016 15:47:11 -0400

haskell-ghc-mtl (1.2.1.0-4) unstable; urgency=medium

  [ Joachim Breitner ]
  * Add Uploaders field, which I accidentally dropped

  [ Clint Adams ]
  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:28 -0500

haskell-ghc-mtl (1.2.1.0-3) experimental; urgency=medium

  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:27:40 +0200

haskell-ghc-mtl (1.2.1.0-2) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:49:51 +0200

haskell-ghc-mtl (1.2.1.0-1) experimental; urgency=low

  * Adjust watch file to new hackage layout
  * Depend on haskell-devscripts 0.9, found in experimental
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sun, 21 Dec 2014 11:07:17 +0100

haskell-ghc-mtl (1.0.1.2-3) unstable; urgency=low

  * Move Haskell blurb to the end of the description, reduces the impact
    of #708703

 -- Joachim Breitner <nomeata@debian.org>  Sat, 25 May 2013 23:52:17 +0200

haskell-ghc-mtl (1.0.1.2-2) unstable; urgency=low

  * Enable compat level 9
  * Use substvars for Haskell description blurbs

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:50:40 +0200

haskell-ghc-mtl (1.0.1.2-1) experimental; urgency=low

  * Depend on haskell-devscripts 0.8.13 to ensure this packages is built
    against experimental
  * Bump standards version, no change
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Thu, 08 Nov 2012 10:30:35 +0100

haskell-ghc-mtl (1.0.1.1-1) unstable; urgency=low

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sat, 25 Feb 2012 16:47:14 +0100

haskell-ghc-mtl (1.0.1.0-3) unstable; urgency=low

  [ Marco Túlio Gontijo e Silva ]
  * debian/source/format: Use 3.0 (quilt).

  [ Marco Silva ]
  * Use ghc instead of ghc6

  [ Iain Lane ]
  * Standards-Version → 3.9.2, no changes required

 -- Iain Lane <laney@debian.org>  Wed, 25 May 2011 11:55:50 +0100

haskell-ghc-mtl (1.0.1.0-2) unstable; urgency=low

  [ Marco Túlio Gontijo e Silva ]
  * debian/watch: Use format that works for --download-current-version.
  * debian/watch: Add .tar.gz to downloaded filename.
  * debian/watch: Include package name in downloaded .tar.gz.
  * debian/watch: Remove spaces, since they're not allowed by uscan.
  * debian/control: Add field Provides: ${haskell:Provides} to -dev and
    -prof packages.
  * Update Uploader e-mail.
  * debian/control: Use Vcs-Browser: field.
  * debian/control: Remove dependency in hscolour, since it's now a
    dependency of haskell-devscripts.
  * debian/control: Remove haddock from Build-Depends:, since it's now a
    Depends: of haskell-devscripts.
  * debian/control: Bump Standards-Version: to 3.8.4, no changes needed.
  * debian/control: Split Build-Depends: to Build-Depends-Indep:.
  * debian/control: Bump version of Build-Depends: on haskell-devscripts
    to 0.7 and remove versioned Build-Depends: on ghc6*.
  * debian/control: Include missing haskell: variables.
  * Use debian/compat 7.
  * debian/control: Use more sintetic link in Homepage:.

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Feb 2010 18:26:50 +0100

haskell-ghc-mtl (1.0.1.0-1) unstable; urgency=low

  * Initial release. (Closes: #514758)

 -- Marco Túlio Gontijo e Silva <marcot@riseup.net>  Fri, 18 Dec 2009 09:57:13 -0200
