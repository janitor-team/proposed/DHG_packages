hothasktags (0.3.8-4) UNRELEASED; urgency=medium

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10

  [ Dmitry Bogatov ]
  * Add missing "Upstream-Name" field into "debian/copyright".

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sat, 29 Sep 2018 14:08:52 +0300

hothasktags (0.3.8-3) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Build-depend on newer libghc-src-exts-dev
  * Update d/copyright file and set license to BSD-3-clause

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Thu, 28 Jun 2018 11:17:38 +0300

hothasktags (0.3.8-2) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:05:12 -0400

hothasktags (0.3.8-1) unstable; urgency=medium

  * New upstream release
  * Add 0001-update-to-haskell-src-exts-1.19.1.patch (Closes: #867260).
    Thanks to Joey Hess for the patch.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 07 Jul 2017 22:53:56 +0100

hothasktags (0.3.7-3) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:37:19 -0400

hothasktags (0.3.7-2) experimental; urgency=medium

  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Mon, 17 Oct 2016 15:21:00 -0400

hothasktags (0.3.7-1) unstable; urgency=medium

  * New upstream release
  * Add Lintian override for hardening-no-relro.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 05 Aug 2016 09:34:28 -0700

hothasktags (0.3.6-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Sean Whitton ]
  * Package new upstream release.
  * Override dh_strip to disable automatic -dbgsym binary.
  * Add myself as an uploader.
  * Remove Joey Hess as an uploader (he has left Debian).

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 19 May 2016 13:35:40 +0900

hothasktags (0.3.5-1) unstable; urgency=medium

  * New upstream release (Closes: #811065)

 -- Joachim Breitner <nomeata@debian.org>  Sun, 31 Jan 2016 19:38:13 +0100

hothasktags (0.3.3-4) unstable; urgency=medium

  [ Joachim Breitner ]
  * This package doesn’t actually use the usual haskell packaging scripts,
    adjusting build-depends (although someone should probably just fix the
    packaging)

  [ Clint Adams ]
  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:55:21 -0500

hothasktags (0.3.3-3) experimental; urgency=medium

  * Add lintian override for spurious rpaths

 -- Joachim Breitner <nomeata@debian.org>  Fri, 21 Aug 2015 09:14:00 +0200

hothasktags (0.3.3-2) experimental; urgency=medium

  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:29:13 +0200

hothasktags (0.3.3-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 29 Jun 2015 11:23:30 +0200

hothasktags (0.3.2-1) unstable; urgency=low

  * New upstream release

 -- Sven Bartscher <sven.bartscher@weltraumschlangen.de>  Tue, 03 Jun 2014 20:43:15 +0200

hothasktags (0.3.1-3) unstable; urgency=medium

  * Patch for compatibility with src-exts 1.14. Closes: #730888
  * Migrate VCS from git to darcs.
  * Bump Standards-Version without any changes.
  * Update watch file

 -- Raúl Benencia <rul@kalgan.cc>  Sat, 01 Mar 2014 17:24:19 -0300

hothasktags (0.3.1-2) unstable; urgency=low

  * Avoid using runghc, which requires working ghci.

 -- Joey Hess <joeyh@debian.org>  Sun, 07 Apr 2013 14:35:38 -0400

hothasktags (0.3.1-1) unstable; urgency=low

  * New upstream release.
  * Avoid running cabal in the build process; among other problems
    cabal writes to $HOME. Closes: #704876

 -- Joey Hess <joeyh@debian.org>  Sun, 07 Apr 2013 13:32:05 -0400

hothasktags (0.3.0-1) unstable; urgency=low

  * Initial release.

 -- Joey Hess <joeyh@debian.org>  Wed, 30 Jan 2013 11:33:04 +1100
