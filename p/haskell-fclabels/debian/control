Source: haskell-fclabels
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-base-orphans-dev (>= 0.8.2),
 libghc-base-orphans-dev (<< 0.9),
 libghc-base-orphans-prof,
Build-Depends-Indep: ghc-doc,
 libghc-base-orphans-doc,
Standards-Version: 4.5.0
Homepage: https://github.com/sebastiaanvisser/fclabels
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-fclabels
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-fclabels]

Package: libghc-fclabels-dev
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}, ${shlibs:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: first-class accessor labels
 This package provides first class labels that can act as
 bidirectional record fields. The labels can be derived
 automatically using Template Haskell which means you don't have
 to write any boilerplate yourself. The labels are implemented as
 lenses and are fully composable. Labels can be used to /get/,
 /set/ and /modify/ parts of a datatype in a consistent way.
 .
 See "Data.Label" for an introductory explanation.
 .
 Internally lenses are not tied to Haskell functions directly,
 but are implemented as arrows. Arrows allow the lenses to be run
 in custom computational contexts. This approach allows us to
 make partial lenses that point to fields of multi-constructor
 datatypes in an elegant way.
 .
 See the "Data.Label.Maybe" module for the use of partial labels.
 .
 > 1.1.1.0 -> 1.1.2
 >   - Added partial set/modify versions that act as identity
 >     when the constructor field is not available.
 .
  Author: Sebastiaan Visser, Erik Hesselink, Chris Eidhof, Sjoerd Visscher
 with lots of help and feedback from others.
  Upstream-Maintainer: Sebastiaan Visser <code@fvisser.nl>
 .
 This package contains the normal library files.

Package: libghc-fclabels-prof
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: first-class accessor labels; profiling libraries
 This package provides first class labels that can act as
 bidirectional record fields. The labels can be derived
 automatically using Template Haskell which means you don't have
 to write any boilerplate yourself. The labels are implemented as
 lenses and are fully composable. Labels can be used to /get/,
 /set/ and /modify/ parts of a datatype in a consistent way.
 .
 See "Data.Label" for an introductory explanation.
 .
 Internally lenses are not tied to Haskell functions directly,
 but are implemented as arrows. Arrows allow the lenses to be run
 in custom computational contexts. This approach allows us to
 make partial lenses that point to fields of multi-constructor
 datatypes in an elegant way.
 .
 See the "Data.Label.Maybe" module for the use of partial labels.
 .
 > 1.1.1.0 -> 1.1.2
 >   - Added partial set/modify versions that act as identity
 >     when the constructor field is not available.
 .
  Author: Sebastiaan Visser, Erik Hesselink, Chris Eidhof, Sjoerd Visscher
 with lots of help and feedback from others.
  Upstream-Maintainer: Sebastiaan Visser <code@fvisser.nl>
 .
 This package contains the libraries compiled with profiling enabled.

Package: libghc-fclabels-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Description: first-class accessor labels; documentation
 This package provides first class labels that can act as
 bidirectional record fields. The labels can be derived
 automatically using Template Haskell which means you don't have
 to write any boilerplate yourself. The labels are implemented as
 lenses and are fully composable. Labels can be used to /get/,
 /set/ and /modify/ parts of a datatype in a consistent way.
 .
 See "Data.Label" for an introductory explanation.
 .
 Internally lenses are not tied to Haskell functions directly,
 but are implemented as arrows. Arrows allow the lenses to be run
 in custom computational contexts. This approach allows us to
 make partial lenses that point to fields of multi-constructor
 datatypes in an elegant way.
 .
 See the "Data.Label.Maybe" module for the use of partial labels.
 .
 > 1.1.1.0 -> 1.1.2
 >   - Added partial set/modify versions that act as identity
 >     when the constructor field is not available.
 .
  Author: Sebastiaan Visser, Erik Hesselink, Chris Eidhof, Sjoerd Visscher
 with lots of help and feedback from others.
  Upstream-Maintainer: Sebastiaan Visser <code@fvisser.nl>
 .
 This package contains the documentation files.
