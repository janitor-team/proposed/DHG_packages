From 6ae35fd5b5f0c5d815455ab43c15a754515137e3 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Florian=20M=C3=A4rkl?= <info@florianmaerkl.de>
Date: Thu, 30 Jul 2020 17:01:59 +0200
Subject: [PATCH] Fix for BoxedObject Change in haskell-gi 0.24

--- a/GI/Cairo/Render/Types.chs
+++ b/GI/Cairo/Render/Types.chs
@@ -1,4 +1,5 @@
 {-# LANGUAGE RecordWildCards #-}
+{-# LANGUAGE DataKinds, TypeFamilies #-}
 {-# OPTIONS_HADDOCK hide #-}
 -----------------------------------------------------------------------------
 -- |
@@ -65,7 +66,8 @@
 
 {#import GI.Cairo.Render.Matrix#}
 
-import Data.GI.Base (ManagedPtr, BoxedObject(..), GType(..))
+import Data.GI.Base.Overloading (ParentTypes, HasParentTypes)
+import Data.GI.Base (ManagedPtr, TypedObject(..), GType(..), GBoxed(..))
 import Foreign hiding (rotate)
 import Foreign.C
 
@@ -79,12 +81,17 @@
   c_cairo_gobject_context_get_type :: IO GType
 
 -- not visible
-newtype Cairo = Cairo (ManagedPtr Cairo) 
+newtype Cairo = Cairo (ManagedPtr Cairo)
 {#pointer *cairo_t as CairoPtr -> Cairo#}
 unCairo (Cairo x) = x
-        
-instance BoxedObject Cairo where
-  boxedType _ = c_cairo_gobject_context_get_type
+
+type instance ParentTypes Cairo = '[]
+instance HasParentTypes Cairo
+
+instance TypedObject Cairo where
+  glibType = c_cairo_gobject_context_get_type
+
+instance GBoxed Cairo
 
 -- | The medium to draw on.
 newtype Surface = Surface { unSurface :: ForeignPtr Surface }
--- a/gi-cairo-render.cabal
+++ b/gi-cairo-render.cabal
@@ -36,7 +36,7 @@
         build-depends:    base >= 4 && < 5,
                           utf8-string >= 0.2 && < 1.1,
                           text >= 1.0.0.0 && < 1.3,
-                          haskell-gi-base >= 0.21.0 && <0.22,
+                          haskell-gi-base >= 0.24.0 && <0.25,
                           bytestring, mtl, array
         build-tools:      c2hs >= 0.28 && <0.30
         exposed-modules:  GI.Cairo.Render
