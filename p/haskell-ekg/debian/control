Source: haskell-ekg
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Iustin Pop <iustin@debian.org>,
 Louis Bettens <louis@bettens.info>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-aeson-dev (>= 0.4),
 libghc-aeson-dev (<< 1.6),
 libghc-aeson-prof,
 libghc-ekg-core-dev (<< 0.2),
 libghc-ekg-core-dev (>= 0.1),
 libghc-ekg-core-prof,
 libghc-ekg-json-dev (<< 0.2),
 libghc-ekg-json-dev (>= 0.1),
 libghc-ekg-json-prof,
 libghc-network-dev (<< 3.2),
 libghc-network-prof,
 libghc-snap-core-dev (<< 1.1),
 libghc-snap-core-prof,
 libghc-snap-server-dev (<< 1.2),
 libghc-snap-server-prof,
 libghc-unordered-containers-dev (<< 0.3),
 libghc-unordered-containers-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-aeson-doc,
 libghc-ekg-core-doc,
 libghc-ekg-json-doc,
 libghc-network-doc,
 libghc-snap-core-doc,
 libghc-snap-server-doc,
 libghc-unordered-containers-doc,
Standards-Version: 4.5.0
Homepage: https://github.com/tibbe/ekg
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-ekg
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-ekg]

Package: libghc-ekg-dev
Architecture: any
Depends:
 libghc-ekg-data (= ${source:Version}),
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: remote monitoring of Haskell processes over HTTP${haskell:ShortBlurb}
 The ekg library lets you remotely monitor a running (Haskell) process
 over HTTP. It provides a simple way to integrate a monitoring server
 into any application.
 .
 ${haskell:Blurb}

Package: libghc-ekg-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: remote monitoring of Haskell processes over HTTP${haskell:ShortBlurb}
 The ekg library lets you remotely monitor a running (Haskell) process
 over HTTP. It provides a simple way to integrate a monitoring server
 into any application.
 .
 ${haskell:Blurb}

Package: libghc-ekg-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: remote monitoring of Haskell processes over HTTP${haskell:ShortBlurb}
 The ekg library lets you remotely monitor a running (Haskell) process
 over HTTP. It provides a simple way to integrate a monitoring server
 into any application.
 .
 ${haskell:Blurb}

Package: libghc-ekg-data
Architecture: all
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: remote monitoring of Haskell processes over HTTP - common files
 The ekg library lets you remotely monitor a running (Haskell) process
 over HTTP. It provides a simple way to integrate a monitoring server
 into any application.
 .
 This package provides the data files needed to use the ekg library
 for the Haskell programming language. See http://www.haskell.org/ for
 more information on Haskell.
